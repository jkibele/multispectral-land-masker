# -*- coding: utf-8 -*-
"""
/***************************************************************************
 LandMask
                                 A QGIS plugin
 Create a land (or water) mask for a multispectral raster.
                              -------------------
        begin                : 2013-05-21
        copyright            : (C) 2013 by Jared Kibele
        email                : jkibele@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from landmaskdialog import LandMaskDialog

#- Add my imports
from raster_handling import *


class LandMask:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = QFileInfo(QgsApplication.qgisUserDbFilePath()).path() + "/python/plugins/landmask"
        # initialize locale
        localePath = ""
        locale = QSettings().value("locale/userLocale").toString()[0:2]

        if QFileInfo(self.plugin_dir).exists():
            localePath = self.plugin_dir + "/i18n/landmask_" + locale + ".qm"

        if QFileInfo(localePath).exists():
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = LandMaskDialog()

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/landmask/icon.png"),
            u"Land Mask", self.iface.mainWindow())
        # connect the action to the run method
        QObject.connect(self.action, SIGNAL("triggered()"), self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&Multispectral Land Masker", self.action)
        
    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&Multispectral Land Masker", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        #- populate the combo box with loaded layers
        self.dlg.initLayerCombobox( self.dlg.ui.inputRasterComboBox, 'key_of_default_layer' )
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            #- get the  layer
            rast_layer = self.dlg.layerFromComboBox( self.dlg.ui.inputRasterComboBox )
            rds = RasterDS( rast_layer )
            # get the filename
            filename = str( self.dlg.ui.outputRasterLineEdit.text() )
            # get the threshold from the dialog
            thresh = float( self.dlg.ui.thresholdDoubleSpinBox.text() )
            # get connectivity threshold
            connthresh = int( self.dlg.ui.connectivitySpinBox.text() )
            #mask_band = rds.ward_cluster_land_mask()
            mask_band = rds.simple_land_mask(threshold=thresh)
            
            # filter out small patches
            mask_band = connectivity_filter(mask_band,threshold=connthresh)
            
            outfile = rds.new_image_from_array(mask_band,outfilename=filename)
            
            if self.dlg.ui.addMaskCheckBox.isChecked():
                fileInfo = QFileInfo(filename)
                baseName = fileInfo.baseName()
                rlayer = QgsRasterLayer(filename, baseName)
                if not rlayer.isValid():
                    #print "Layer failed to load!"
                    mbox = QMessageBox()
                    outtext = "Layer failed to load!"
                    mbox.setText( outtext )
                    mbox.setWindowTitle( "Error" )
                    mbox.exec_()
                QgsMapLayerRegistry.instance().addMapLayer(rlayer)
