# -*- coding: utf-8 -*-
"""
/***************************************************************************
 LandMask
                                 A QGIS plugin
 Create a land (or water) mask for a multispectral raster.
                             -------------------
        begin                : 2013-05-21
        copyright            : (C) 2013 by Jared Kibele
        email                : jkibele@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


def name():
    return "Multispectral Land Masker"


def description():
    return "Create a land (or water) mask for a multispectral raster."


def version():
    return "Version 0.1"


def icon():
    return "icon.png"


def qgisMinimumVersion():
    return "1.8"

def author():
    return "Jared Kibele"

def email():
    return "jkibele@gmail.com"

def classFactory(iface):
    # load LandMask class from file LandMask
    from landmask import LandMask
    return LandMask(iface)
