# -*- coding: utf-8 -*-
"""
/***************************************************************************
 LandMaskDialog
                                 A QGIS plugin
 Create a land (or water) mask for a multispectral raster.
                             -------------------
        begin                : 2013-05-21
        copyright            : (C) 2013 by Jared Kibele
        email                : jkibele@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
from ui_landmask import Ui_LandMask

# next imports added manually:
import os
from qgis.core import QgsMapLayerRegistry, QgsMapLayer

class LandMaskDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        # Set up the user interface from Designer.
        self.ui = Ui_LandMask()
        self.ui.setupUi(self)
    
    #- everything below added manually:
    def initLayerCombobox(self,combobox, default):
         combobox.clear()
         reg = QgsMapLayerRegistry.instance()
         for ( key, layer ) in reg.mapLayers().iteritems():
             
             if layer.type() == QgsMapLayer.RasterLayer and ( layer.usesProvider() and layer.providerKey() == 'gdal' ):
                 combobox.addItem( layer.name(), key )
         
         idx = combobox.findData( default )
         if idx != -1:
             combobox.setCurrentIndex( idx ) 
             
    def layerFromComboBox(self, combobox):
        layerID = combobox.itemData(combobox.currentIndex()).toString()
        return QgsMapLayerRegistry.instance().mapLayer( layerID )
        
    def showFileSelectDialog(self):
        fname = QtGui.QFileDialog.getSaveFileName(self, 'Save File', os.path.expanduser('~'))
        self.ui.outputRasterLineEdit.setText( fname )